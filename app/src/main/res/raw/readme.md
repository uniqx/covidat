CovidAt
=======

Une application Android de génération d'attestation de déplacement dérogatoire dans le cadre du confinement en France lié à l'épidémie de Covid-19
par Codefish <codefish@online.fr>


Cette application permet de créer des attestations a priori conformes au modèle proposé par [le ministère de l'intérieur français sur son site](https://media.interieur.gouv.fr/deplacement-covid-19/).
La version du ministère utilise des technologies web (HTML/JavaScript) pour générer localement par le navigateur une attestation (les requêtes distantes sont réalisées pour récupérer le modèle de l'attestation et le code du générateur).

CovidAt propose un générateur utilisant les APIs Android natives et n'utilisant pas la permission `INTERNET` : aucune communication extérieure n'est réalisée. Tout est réalisé localement sur l'appareil. La génération d'une attestation peut être automatisée par un service lorsque l'on quitte son domicile (perte du signal wifi domestique).

CovidAt fonctionne à partir d'Android 4.4 (KitKat) : cette version introduit l'API de génération de PDF.

Fonctionnalités
---------------

* Génération d'attestation en fournissant ses données personnelles (identité, domiciliation) ainsi que les motifs de déplacement
* Attestation générée sous la forme d'un fichier PDF intégrant un QR-code
* Visualisation directe de l'attestation et du QR-code depuis l'application
* Visualisation du fichier PDF par une application externe
* Export du fichier PDF dans un fichier
* Sauvegarde du fichier PDF dans un fichier
* Service en avant-plan de génération automatique en sortant du domicile avec notification intégrant les principales informations et le QR-code (accessible directement depuis l'écran de verrouillage)
* Alertes sonores de génération automatique d'attestation, de batterie faible et de durée de sortie dépassant 1h ou de distance supérieure à 1 km (pour des sorties sportives)

Permissions
-----------

 Les permissions utilisées sont les suivantes :
- `ACCESS_{WIFI_STATE, NETWORK_STATE}` pour connaître l'état de la connexion wifi (utilisé pour le service de génération)
- `ACCESS_FINE_LOCATION` pour récupérer l'identité du point d'accès wifi utilisé ; CovidAt utilise uniquement cette permission pour savoir si le réseau wifi actuel est bien le réseau domestique. L'utilisateur n'a pas besoin d'accorder cette permission si le service de génération n'est pas utilisé
- `FOREGROUND_SERVICE` pour autoriser le service à être en avant-plan (afficher une notification permanente)
- `ON_BOOT_COMPLETED` pour lancer automatiquement le service au démarrage du système

Toutes ces permissions se rapportent au service de génération par sortie de portée du réseau wifi domestique. La génération d'une attestation par l'activité graphique ne requiert aucune permission.

L'application n'envoie aucune donnée personnelle sur Internet ; elle ne possède pas la permission `INTERNET`.

Garanties
---------

Malgré tout le soin apporté à son développement, l'application est fournie sans aucune garantie ; l'auteur ne sera aucunement responsable d'un dysfonctionnement quel qu'il soit et des conséquences qui en résulteraient (telle qu'une verbalisation).

Pour plus de sûreté, il est recommandé de générer manuellement ses attestations, de vérifier leur bon contenu et de les exporter vers un fichier externe pouvant être ouvert par une autre application (lecteur PDF). Ainsi l'attestation reste accessible même en cas de plantage de l'application. D'autre part, il faut penser à avoir son appareil avec un niveau de charge suffisant.

Bibliothèques
-------------

Cette application utilise les bibliothèques suivantes :
* Bibliothèques standard d'Android et paquetages androidx (proposées par Google sous licence Apache 2.0
* Composant [`PhotoView`](https://github.com/chrisbanes/PhotoView) de Chris Banes pour la visualisation zoomable d'attestations (sous licence Apache 2.0)
* Bibliothèque [ZXing](https://github.com/zxing/zxing) pour la génération de QR codes (sous licence Apache 2.0)
* Bibliothèque [GSon](https://github.com/google/gson) de Google pour la (dé)sérialisation d'objets Java en JSON
* Bibliothèque [CommonMark](https://commonmark.org/) d'Atlassian pour la conversion de texte formatté en MarkDown vers HTML (sous licence BSD)
* Bibliothèque [SpinnerDatePicker](https://github.com/drawers/SpinnerDatePicker) de David Rawson pour proposer un dialogue de saisie de date de naissance utilisant des NumberPicker (et pas un CalendarView) (sous licence Apache 2.0)

Licence
-------

Cette application est disponible sous [licence CeCILL 2.1](https://cecill.info/licences.fr.html) qui autorise la redistribution de l'application (et de versions dérivées) sous réserve de fournir son code-source et de garder la mention de l'auteur original.