/*
 * Copyright (c) 2020 Codefish <codefish@online.fr>
 * Software released under the terms of the CeCILL 2.1 license [https://cecill.info/]
 */

package fr.covidat.android.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import fr.covidat.android.R
import fr.covidat.android.service.GeneratorService
import fr.covidat.android.generation.Attestation
import fr.covidat.android.generation.AttestationGeneratorFragmentDirections
import fr.covidat.android.utils.SoundPlayer
import java.io.IOException

/**
 * The main activity of the application hosting the fragments
 */
class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(R.id.nav_generator, R.id.nav_attestation, R.id.nav_qrcode, R.id.nav_profile, R.id.nav_service, R.id.nav_about), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        val action = AttestationGeneratorFragmentDirections.gotoAttestationFromMain()
        val goto = intent.getIntExtra("goto", -1)
        if (goto != -1 && savedInstanceState == null)
            navController.navigate(action)

        GeneratorService.start(this)
    }

    override fun onStart() {
        super.onStart()
        SoundPlayer.active = false
    }

    override fun onStop() {
        super.onStop()
        SoundPlayer.active = true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.options, menu)
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        val attestation = Attestation.getLatestAttestation(this)
        arrayOf(R.id.viewAttestationMenuItem, R.id.shareAttestationMenuItem, R.id.saveAttestationMenuItem).forEach {
            menu?.findItem(it)?.isEnabled = attestation != null
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val attestation = Attestation.getLatestAttestation(this)
        when(item.itemId) {
            R.id.viewAttestationMenuItem -> attestation?.launchPDFIntent(this, Intent.ACTION_VIEW)
            R.id.shareAttestationMenuItem -> attestation?.launchPDFIntent(this, Intent.ACTION_SEND)
            R.id.saveAttestationMenuItem -> saveAttestation()
            else -> return false
        }
        return true
    }

    fun saveAttestation() {
        Intent(Intent.ACTION_CREATE_DOCUMENT)
            .setType("application/pdf")
            .apply { startActivityForResult(this, CREATE_PDF_REQUEST_CODE) }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CREATE_PDF_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val uri = data?.data
            uri?.let {
                val attestation = Attestation.getLatestAttestation(this)
                try {
                    attestation?.copyPDFAttestation(uri)
                    Toast.makeText(this, "Copie réussie", Toast.LENGTH_LONG).show()
                } catch (e: IOException) {
                    Log.e(javaClass.name, "Error while copying", e)
                    Toast.makeText(this, "Erreur lors de la copie: " + e, Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    companion object {
        const val CREATE_PDF_REQUEST_CODE = 1
    }
}
