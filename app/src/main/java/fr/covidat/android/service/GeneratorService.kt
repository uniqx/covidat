/*
 * Copyright (c) 2020 Codefish <codefish@online.fr>
 * Software released under the terms of the CeCILL 2.1 license [https://cecill.info/]
 */

package fr.covidat.android.service

import android.app.*
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.os.Build
import android.os.Handler
import android.os.IBinder
import android.os.SystemClock
import android.util.Log
import android.widget.Toast
import androidx.core.app.AlarmManagerCompat
import androidx.core.app.NotificationCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import fr.covidat.android.R
import fr.covidat.android.activities.MainActivity
import fr.covidat.android.generation.*
import fr.covidat.android.model.AttestationData
import fr.covidat.android.model.Reason
import fr.covidat.android.utils.*
import java.lang.UnsupportedOperationException


/** A foreground service that auto-generates an attestation when we leave the domestic wifi network */
class GeneratorService : Service() {

    companion object {
        const val MIN_HOME_NETWORK_DURATION = 30 * 1000L // 30 seconds
        const val CHANNEL_ID = "defaultChannel"
        const val NOTIFICATION_ID = 1
        const val DURATION_ALARM_DELAY = 60 * 60 * 1000L // one hour in millis
        const val PROXIMITY_ALARM_DISTANCE = 1000.0f // one kilometer in meters
        const val LOW_BATTERY_THRESHOLD = 0.25f
        val DURATION_ALARM_ACTION = javaClass.name + ".duration_alarm_action"

        fun start(context: Context, startForeground: Boolean = false) {
            try {
                val config = loadGeneratorServiceConfig(context)
                Intent(context, GeneratorService::class.java).let {
                    if (startForeground && config.activated && Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                        context.startForegroundService(it)
                    else
                        context.startService(it)
                }
            } catch (e: Exception) {
                Log.e(javaClass.name, "Cannot start service", e)
            }
        }

        fun areSameMac(mac1: String?, mac2: String?): Boolean {
            if (mac1 == null || mac2 == null) return false
            return mac1 == mac2
        }
    }

    override fun onBind(intent: Intent): IBinder {
        throw UnsupportedOperationException()
    }

    var started = false
    var config: GeneratorServiceConfig =
        GeneratorServiceConfig()
    var connectionState: WifiConnection? = null
    var latestAttestationData: AttestationData? = null

    var proximityInformer: ProximityInformer? = null
    var proximityAlarmTimestamp: Long = -1L

    val handler: Handler by lazy { Handler() }

    fun onNetworkEvent() {
        val oldConnectionState = connectionState
        val newConnectionState = wifiConnection
        if (config.activated && oldConnectionState != null
            && oldConnectionState.state == WifiConnectionState.CONNECTED && config.isMatchingWifiName(oldConnectionState.ssid)
            && !config.isMatchingWifiName(newConnectionState.ssid)
            && oldConnectionState.duration >= MIN_HOME_NETWORK_DURATION) {
            Log.i(javaClass.name, "Leaving domestic network")
                onHomeLeave()
            }
        connectionState = newConnectionState
    }

    /** Generate an attestation when we leave home */
    fun onHomeLeave() {
        Log.i(javaClass.name, "Leaving home, generating a new attestation...")
        AttestationData.create(this)?.let { AttestationGeneratorTask(this).execute(it) }
    }

    fun updateNotification(generated: Boolean = false) {
        val nm = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        nm.notify(NOTIFICATION_ID, createNotification(generated))
    }

    /** Receiver for network events */
    val networkReceiver = object: BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) { onNetworkEvent() }
    }

    fun triggerSoundAlarm() {
        val homeNetwork = config.isMatchingWifiName(connectionState?.ssid)
        if (! homeNetwork)
            config.durationToneUri?.let { SoundPlayer.playSound(this@GeneratorService, it) }
    }

    /** Receiver for attestation generation */
    val attestationReceiver = object: BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            latestAttestationData = intent.getSerializableExtra("attestationData") as? AttestationData
            updateNotification(true)
        }
    }

    val durationReceiver = object: BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            Log.i(javaClass.name, "Duration alarm is triggered")
            triggerSoundAlarm()
            proximityInformer?.close()
            proximityInformer = null
            updateNotification(false)
        }
    }

    val alarmManager by lazy { getSystemService(Context.ALARM_SERVICE) as AlarmManager }
    val alarmPendingIntent by lazy { PendingIntent.getBroadcast(this, 1, Intent(DURATION_ALARM_ACTION), 0) }

    fun installDurationAlarm() {
        val timestamp = latestAttestationData?.circumstances?.date
        if (timestamp != null) {
            val deadline = timestamp + DURATION_ALARM_DELAY
            if (deadline > System.currentTimeMillis()) {
                AlarmManagerCompat.setExactAndAllowWhileIdle(alarmManager, AlarmManager.RTC_WAKEUP, deadline, alarmPendingIntent)
            }
        }
    }

    val distanceListener: (Float) -> Unit = {
        if (proximityAlarmTimestamp == -1L && it > PROXIMITY_ALARM_DISTANCE) {
            proximityAlarmTimestamp = SystemClock.elapsedRealtime()
            Log.i(javaClass.name, "Proximity alarm is triggered")
            triggerSoundAlarm()
        }
        updateNotification(false)
    }

    fun installProximityAlarm() {
        proximityAlarmTimestamp = -1L
        proximityInformer = ProximityInformer.create(this, distanceListener)
        Log.i(javaClass.name, "Proximity informer status: working=${proximityInformer?.working}")
    }

    fun onReceivedIntent() {
        config = loadGeneratorServiceConfig(this)
        if (! config.activated) stopSelf()
        if (! started) {
            Log.i(javaClass.name, "Starting service")
            LocalBroadcastManager.getInstance(this).registerReceiver(attestationReceiver,
                IntentFilter(AttestationTaskResult.NEW_ATTESTATION_ACTION))
            registerReceiver(networkReceiver, IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"))
            registerReceiver(durationReceiver, IntentFilter(DURATION_ALARM_ACTION))
            started = true
            createNotificationChannel()
            latestAttestationData = Attestation.getLatestAttestation(this)?.attestationData
            startForeground(NOTIFICATION_ID, createNotification())
            onNetworkEvent() // induce the first network event
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        try {
            onReceivedIntent()
            return START_REDELIVER_INTENT
        } catch (e: Exception) {
            Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show()
            Log.e(javaClass.name, "Exception when starting service, disabling it", e)
            GeneratorServiceConfig(activated = false).save(this)
            stopSelf()
            return START_NOT_STICKY
        }
    }

    /** This method creates a new notification channel (required for API 26+)
     * It is copied from https://developer.android.com/training/notify-user/build-notification
     */
    private fun createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = "COVID-19 attestation channel"
            val description = "Canal de notification pour le générateur d'attestations"
            val importance = NotificationManager.IMPORTANCE_HIGH
            val channel = NotificationChannel(CHANNEL_ID, name, importance)
            channel.description = description
            channel.enableVibration(true)
            channel.shouldVibrate()
            channel.setSound(null, null)
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            val notificationManager = getSystemService(NotificationManager::class.java)
            notificationManager!!.createNotificationChannel(channel)
        }
    }

    private fun getNotificationTitle(): String {
        val d = latestAttestationData
        if (d == null)
            return "Aucune attestation de déplacement"
        else
            return "Attestation ${d.circumstances.reasonsText} de ${d.personalData.firstName} ${d.personalData.lastName}"
    }

    private fun getNotificationContent(): String? {
        val d = latestAttestationData?.personalData
        if (d == null) return null
        else return "Résidant ${d.street} à ${d.city}"
    }

    val isBatteryLow: Boolean get() = batteryLevel < LOW_BATTERY_THRESHOLD

    private fun getNotificationSubText(): String? {
        val d = proximityInformer?.currentDistance
        return if (isBatteryLow) "Niveau de batterie bas !"
        else if (d != null) "${d.toInt()} m"
        else null
    }

    private fun createNotification(generated: Boolean = false): Notification {
        var priority = NotificationCompat.PRIORITY_DEFAULT
        val mBuilder = NotificationCompat.Builder(this,
            CHANNEL_ID
        )
            .setSmallIcon(R.drawable.ic_stat_name)
            .setContentTitle(getNotificationTitle())

        val data = latestAttestationData
        if (data != null) {
            mBuilder.setContentText(getNotificationContent())
            mBuilder.setUsesChronometer(true)
            mBuilder.setWhen(data.circumstances.date)
            mBuilder.setShowWhen(true)
        }
        if (generated) {
            config.getToneUri(this)?.let { SoundPlayer.playSound(this, it) }
            proximityInformer?.close()
            alarmManager.cancel(alarmPendingIntent)
            priority = NotificationCompat.PRIORITY_HIGH
        }

        getNotificationSubText()?.let { mBuilder.setSubText(it) }

        try {
            Attestation.getLatestAttestation(this)?.generateQRCode(512 to 256)?.let {
                mBuilder.setStyle(NotificationCompat.BigPictureStyle().bigPicture(it))
            }
        } catch (e: Exception) {
            Log.e(javaClass.name, "Cannot load the bitmap to the notification", e)
        }

        val sport = data != null && Reason.sport in data.circumstances.reasons

        if (sport) {
            if (generated && config.durationTone != null) {
                installDurationAlarm()
                if (!isBatteryLow) installProximityAlarm()
            } else if (data?.circumstances?.duration ?: 0L > DURATION_ALARM_DELAY || proximityInformer?.currentDistance ?: 0.0f > PROXIMITY_ALARM_DISTANCE) {
                mBuilder.color = Color.RED
                mBuilder.setColorized(true)
                priority = NotificationCompat.PRIORITY_HIGH
            }
        }

        if (priority != NotificationCompat.PRIORITY_HIGH)
            mBuilder.setOnlyAlertOnce(true)

        // Associate an action to the notification to start a linked Activity
        val resultIntent = Intent(this, MainActivity::class.java)
        resultIntent.putExtra("goto", R.id.nav_attestation) // view the attestation
        // do not start the activity again if it is already on top
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
        mBuilder.setContentIntent(PendingIntent.getActivity(this, 0, resultIntent, 0))
        mBuilder.priority = priority
        return mBuilder.build()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (started) {
            stopForeground(true)
            LocalBroadcastManager.getInstance(this).unregisterReceiver(attestationReceiver)
            unregisterReceiver(networkReceiver)
            unregisterReceiver(durationReceiver)
            proximityInformer?.close()
        }
    }
}
