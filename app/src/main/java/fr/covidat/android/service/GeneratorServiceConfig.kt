/*
 * Copyright (c) 2020 Codefish <codefish@online.fr>
 * Software released under the terms of the CeCILL 2.1 license [https://cecill.info/]
 */

package fr.covidat.android.service

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.net.Uri
import android.preference.PreferenceManager
import androidx.core.content.ContextCompat
import com.google.gson.Gson
import fr.covidat.android.utils.batteryLevel
import fr.covidat.android.utils.wildcardToRegex

data class GeneratorServiceConfig(
    val activated: Boolean = false,
    val wifiName: String? = null,
    val generationTone: String? = null,
    val batteryTone: String? = null,
    val durationTone: String? = null) {

    val generationToneUri: Uri? get() = if (generationTone != null) Uri.parse(generationTone) else null
    val batteryToneUri: Uri? get() = if (batteryTone != null) Uri.parse(batteryTone) else null
    val durationToneUri: Uri? get() = if (durationTone != null) Uri.parse(durationTone) else null

    fun getToneUri(context: Context) = if (batteryToneUri != null
        && context.batteryLevel <= GeneratorService.LOW_BATTERY_THRESHOLD) batteryToneUri
    else generationToneUri

    /** Check also if the writing permission is granted */
    fun isReallyActivated(activity: Activity): Boolean {
        return activated && ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
    }

    fun isMatchingWifiName(value: String?) =
        value != null && wifiName?.trim()?.isNotEmpty() == true && wifiName.wildcardToRegex.matches(value) == true
}

private val gson = Gson()

fun GeneratorServiceConfig.save(context: Context) {
    val editor = PreferenceManager.getDefaultSharedPreferences(context).edit()
    editor.putString("generatorServiceConfig", gson.toJson(this))
    editor.commit()
}

fun loadGeneratorServiceConfig(context: Context): GeneratorServiceConfig {
    val s = PreferenceManager.getDefaultSharedPreferences(context).getString("generatorServiceConfig", null)
    return s?.let {
        try {
            gson.fromJson(it, GeneratorServiceConfig::class.java)
        } catch (e: Exception) { null } } ?: GeneratorServiceConfig()
}