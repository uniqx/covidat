/*
 * Copyright (c) 2020 Codefish <codefish@online.fr>
 * Software released under the terms of the CeCILL 2.1 license [https://cecill.info/]
 */

package fr.covidat.android.service


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import fr.covidat.android.R
import kotlinx.android.synthetic.main.fragment_service.*
import android.media.RingtoneManager
import android.content.Intent
import android.app.Activity
import android.app.Dialog
import android.net.Uri
import android.text.method.ScrollingMovementMethod
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import fr.covidat.android.utils.WifiConnectionState
import fr.covidat.android.utils.pickTone
import fr.covidat.android.utils.wifiConnection


/**
 * A fragment to configure the generator service
 */
class GeneratorServiceFragment : Fragment() {

    companion object {
        const val NOTIFICATION_TONE_REQUEST_CODE = 1
        const val BATTERY_TONE_REQUEST_CODE = 2
        const val DURATION_TONE_REQUEST_CODE = 3
    }

    fun activate() {
        val config = loadGeneratorServiceConfig(context!!)
        val c = context!!.wifiConnection
        val newWifiName = when(c.state) {
            WifiConnectionState.CONNECTED -> c.ssid
            WifiConnectionState.UNAUTHORIZED -> null
            else -> config.wifiName
        }
        config.copy(activated = true, wifiName = newWifiName).save(context!!)
        refresh()
    }

    fun refresh() {
        val config = loadGeneratorServiceConfig(context!!)
        Log.v(javaClass.name, "Loading the config $config")
        serviceCheckBox.isChecked = config.activated
        serviceToneCheckBox.isChecked = config.generationTone != null
        batteryToneCheckBox.isChecked = config.batteryTone != null
        durationToneCheckBox.isChecked = config.durationTone != null
        arrayOf(serviceToneCheckBox, batteryToneCheckBox, durationToneCheckBox, modifyNetworkButton).forEach {
            it.isEnabled = serviceCheckBox.isChecked
        }
        wifiNameTextView.text = config.wifiName ?: "non défini"
        modifyNetworkButton.isEnabled = context!!.wifiConnection.state != WifiConnectionState.UNAUTHORIZED
        GeneratorService.start(context!!)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_service, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        explanationTextView.setText(resources.openRawResource(R.raw.service_explanation).bufferedReader().use { it.readText() })
        explanationTextView.setMovementMethod(ScrollingMovementMethod())
        serviceCheckBox.setOnClickListener {
            if (! serviceCheckBox.isChecked) {
                loadGeneratorServiceConfig(context!!).copy(activated = false).save(context!!)
            } else {
                if (! loadGeneratorServiceConfig(context!!).copy(activated = true).isReallyActivated(activity!!)) {
                    requestPermissions(arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), 1)
                } else {
                    activate()
                }
            }
            refresh()
        }
        serviceToneCheckBox.setOnClickListener {
            val config = loadGeneratorServiceConfig(context!!)
            if (serviceToneCheckBox.isChecked)
                pickTone("Choix de la notification sonore de génération d'attestation", NOTIFICATION_TONE_REQUEST_CODE, existingUri = config.generationToneUri )
            else {
                config.copy(generationTone = null).save(context!!)
                refresh()
            }
        }
        batteryToneCheckBox.setOnClickListener {
            val config = loadGeneratorServiceConfig(context!!)
            if (batteryToneCheckBox.isChecked)
                pickTone("Choix de l'alerte batterie faible", BATTERY_TONE_REQUEST_CODE, existingUri = config.batteryToneUri, type = RingtoneManager.TYPE_ALARM )
            else {
                config.copy(batteryTone = null).save(context!!)
                refresh()
            }
        }
        durationToneCheckBox.setOnClickListener {
            val config = loadGeneratorServiceConfig(context!!)
            if (durationToneCheckBox.isChecked)
                pickTone("Choix de l'alerte de durée 1h", DURATION_TONE_REQUEST_CODE, existingUri = config.durationToneUri, type = RingtoneManager.TYPE_ALARM )
            else {
                config.copy(durationTone = null).save(context!!)
                refresh()
            }
        }

        modifyNetworkButton.setOnClickListener {
            val dialog = WifiNetworkDialogFragment()
            dialog.setTargetFragment(this, 1)
            dialog.show(fragmentManager!!, "wifiName")
        }
    }

    override fun onStart() {
        super.onStart()
        explanationTextView.scrollY = 0
        refresh()
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        activate()
        /*if (grantResults.getOrNull(0) == PackageManager.PERMISSION_GRANTED)
            activate()*/
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            val uri = data?.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI) as? Uri
            val config = loadGeneratorServiceConfig(context!!)
            when(requestCode) {
                NOTIFICATION_TONE_REQUEST_CODE -> config.copy(generationTone = uri.toString())
                BATTERY_TONE_REQUEST_CODE -> config.copy(batteryTone = uri.toString())
                DURATION_TONE_REQUEST_CODE -> config.copy(durationTone = uri.toString())
                else -> null
            }?.save(context!!)
        }
        refresh()
    }


    class WifiNetworkDialogFragment : DialogFragment() {
        lateinit var initialName: String

        override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
            return activity?.let {
                // Use the Builder class for convenient dialog construction
                val builder = AlertDialog.Builder(it)
                val input = EditText(context!!)
                val config = loadGeneratorServiceConfig(context!!)
                input.setText(config.wifiName)
                builder.setTitle("Nom du réseau wifi domestique")
                    .setMessage("Indiquez le nom de votre réseau domestique (* peut se substituer à une suite de caractères)")
                    .setView(input)
                    .setPositiveButton(android.R.string.ok) { dialog, id ->
                        val config = loadGeneratorServiceConfig(context!!)
                        config.copy(wifiName = input.text.toString()).save(context!!)
                        (targetFragment as GeneratorServiceFragment).refresh()
                    }
                    .setNegativeButton(android.R.string.cancel) { _,_ -> dismiss() }
                // Create the AlertDialog object and return it

                builder.create()
            } ?: throw IllegalStateException("Activity cannot be null")
        }
    }



}
