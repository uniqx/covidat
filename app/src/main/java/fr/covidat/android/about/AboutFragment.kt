/*
 * Copyright (c) 2020 Codefish <codefish@online.fr>
 * Software released under the terms of the CeCILL 2.1 license [https://cecill.info/]
 */

package fr.covidat.android.about


import android.os.Bundle
import android.text.Html
import android.text.method.LinkMovementMethod
import android.text.util.Linkify
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.view.setPadding
import fr.covidat.android.R
import fr.covidat.android.utils.md2html

/**
 * A fragment displaying an about TextView
 */
class AboutFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return TextView(activity).apply {
            val text = resources.openRawResource(R.raw.readme).bufferedReader().use { it.readText() }
            val license = resources.openRawResource(R.raw.license).bufferedReader().use { it.readText() }
            val html = Html.fromHtml("$text\n\n$license".md2html())
            setText(html)
            setMovementMethod(LinkMovementMethod.getInstance())
            setPadding(8)
            autoLinkMask = Linkify.ALL
        }
    }


}
