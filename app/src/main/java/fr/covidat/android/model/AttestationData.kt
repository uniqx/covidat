/*
 * Copyright (c) 2020 Codefish <codefish@online.fr>
 * Software released under the terms of the CeCILL 2.1 license [https://cecill.info/]
 */

package fr.covidat.android.model

import android.content.Context
import android.preference.PreferenceManager
import com.google.gson.Gson
import fr.covidat.android.generation.Attestation
import java.io.Serializable
import java.lang.Exception
import java.util.*

enum class Reason(val description: String) {
    travail("Déplacement professionnel"),
    courses("Courses de premières nécessités"),
    sante("Urgence médicale"),
    famille("Motif familial impérieux"),
    sport("Activité physique (1 km, 1h max)"),
    judiciaire("Convocation judiciaire"),
    missions("Mission d'intérêt général")
}


/** Circumstances for the attestation */
data class Circumstances(
    val reasons: EnumSet<Reason>,
    val date: Long // timestamp in millis of the date
): Serializable {

    private var _calendar: Calendar? = null

    private val calendar: Calendar get() {
        val c = _calendar
        if (c != null) return c
        else {
            val c = Calendar.getInstance()
            c.timeInMillis = date
            _calendar = c
            return c
        }
    }

    val hour get() = calendar[Calendar.HOUR_OF_DAY].toString().padStart(2, '0')
    val minute get() = calendar[Calendar.MINUTE].toString().padStart(2, '0')
    val hourMinute get() = "${hour}h$minute"
    val day get() = "${calendar[Calendar.DAY_OF_MONTH].toString().padStart(2, '0')}/${(calendar[Calendar.MONTH] + 1).toString().padStart(2, '0')}/${calendar[Calendar.YEAR]}"

    val reasonsText get() = reasons.map { it.toString() }.joinToString("-")
    val duration get() = System.currentTimeMillis() - date
}

data class AttestationData(
    val personalData: PersonalData,
    val circumstances: Circumstances
): Serializable
{
    companion object {
        fun create(context: Context, reasons: EnumSet<Reason>? = null): AttestationData? {
            val personalData = loadPersonalData(context)
            if (personalData == null) return null
            val r = if (reasons != null) reasons else {
                // we load the reasons from the latest attestation
                Attestation.getLatestAttestation(context)?.attestationData?.circumstances?.reasons
            }
            if (r == null) return null
            return AttestationData(
                personalData,
                Circumstances(r, System.currentTimeMillis())
            )
        }
    }

}

private val gson = Gson()


fun loadReasons(context: Context): EnumSet<Reason> {
    val s = PreferenceManager.getDefaultSharedPreferences(context).getString("reasons", null)
    val reasons = EnumSet.noneOf(Reason::class.java)
    if (s != null)
        s.split("-").map { try { Reason.valueOf(it) } catch (e: Exception) {null} }.filterNotNull().forEach { reasons.add(it) }
    return reasons
}

fun EnumSet<Reason>.save(context: Context) {
    val editor = PreferenceManager.getDefaultSharedPreferences(context).edit()
    editor.putString("reasons", this.map { it.name }.joinToString("-"))
    editor.commit()
}