/*
 * Copyright (c) 2020 Codefish <codefish@online.fr>
 * Software released under the terms of the CeCILL 2.1 license [https://cecill.info/]
 */

package fr.covidat.android.model

import android.content.Context
import android.preference.PreferenceManager
import android.util.Log
import com.google.gson.Gson
import java.io.Serializable
import java.lang.Exception

/** Immutable personal data */
data class PersonalData(
    val firstName: String = "",
    val lastName: String = "",
    val birthDate: String = "",
    val birthPlace: String = "",
    val street: String = "",
    val city: String = "",
    val zipcode: String = ""
): Serializable {
    override fun toString() =
        "${firstName} ${lastName}\nné(e) le ${birthDate} à ${birthPlace}\nrésidant ${street}\n${zipcode} ${city}"

    /** Are the supplied data complete and valid? */
    val isValid get() =
        isValidFirstName(firstName) == null &&
                isValidLastName(lastName) == null &&
                isValidBirthDate(birthDate) == null &&
                isValidBirthPlace(birthPlace) == null &&
                isValidStreet(street) == null &&
                isValidCity(city) == null &&
                isValidZipcode(zipcode) == null

    companion object {
        fun isValidFirstName(s: String) = if (s.isBlank()) "prénom vide" else null

        fun isValidLastName(s: String) = if (s.isBlank()) "nom vide" else null

        fun isValidBirthDate(s: String): String? {
            val splitted = s.split("-", "/").map { it.toIntOrNull() }
            val day = splitted.getOrNull(0)
            val month = splitted.getOrNull(1)
            val year = splitted.getOrNull(2)
            if (day == null || month == null || year == null) return "date de naissance invalide"
            if (year < 1900) return "année invalide"
            if (month !in 1..12) return "mois invalide"
            if (month in arrayOf(1, 3, 5, 7, 8, 10, 12) && day !in 1..31) return "jour invalide"
            if (month in arrayOf(4, 6, 9, 11) && day !in 1..30) return "jour invalide"
            if (month == 2 && day !in 1..29) return "jour invalide"
            return null
        }

        fun isValidBirthPlace(s: String) = if (s.isBlank()) "lieu de naissance vide" else null

        fun isValidStreet(s: String) = if (s.isBlank()) "rue vide" else null

        fun isValidCity(s: String) = if (s.isBlank()) "ville vide" else null

        fun isValidZipcode(s: String) = if (s.isBlank()) "code postal vide" else null

    }
}

private val gson = Gson()

/** Load the personal data from the preferences */
fun loadPersonalData(context: Context): PersonalData {
    val s = PreferenceManager.getDefaultSharedPreferences(context).getString("personalData", null)
    return s?.let { try {
        gson.fromJson(it, PersonalData::class.java)
    } catch (e: Exception) { PersonalData() } } ?: PersonalData()
}

/** Save the personal date to the preferences */
fun PersonalData.save(context: Context) {
    val editor = PreferenceManager.getDefaultSharedPreferences(context).edit()
    editor.putString("personalData", gson.toJson(this))
    editor.commit()
}