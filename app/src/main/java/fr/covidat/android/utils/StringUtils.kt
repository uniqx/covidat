/*
 * Copyright (c) 2020 Codefish <codefish@online.fr>
 * Software released under the terms of the CeCILL 2.1 license [https://cecill.info/]
 */

package fr.covidat.android.utils

import org.commonmark.parser.Parser
import org.commonmark.renderer.html.HtmlRenderer


/** Convert markdown to html */
fun String.md2html(): String {
    val parser = Parser.builder().build()
    val document = parser.parse(this)
    val renderer = HtmlRenderer.builder().build()
    return renderer.render(document)
}

/** Convert a string containing star wildcards to a regexp */
val String.wildcardToRegex: Regex get() {
    val literals = this.split('*')
    return literals.mapIndexed { index, s ->
        if (index != literals.size - 1) listOf(Regex.escape(s), ".*")
        else listOf(Regex.escape(s))
    }.flatten().joinToString("").toRegex()
}