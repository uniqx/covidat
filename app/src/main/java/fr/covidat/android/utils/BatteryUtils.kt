/*
 * Copyright (c) 2020 Codefish <codefish@online.fr>
 * Software released under the terms of the CeCILL 2.1 license [https://cecill.info/]
 */

package fr.covidat.android.utils

import android.content.Context
import android.os.BatteryManager
import android.content.Intent
import android.content.IntentFilter


val Context.batteryLevel: Float get() {
    val batteryIntent = registerReceiver(null, IntentFilter(Intent.ACTION_BATTERY_CHANGED))
    val level = batteryIntent?.getIntExtra(BatteryManager.EXTRA_LEVEL, -1)
    val scale = batteryIntent?.getIntExtra(BatteryManager.EXTRA_SCALE, -1)
    if (level == null || scale == null) return 0.5f
    return if (level == -1 || scale == -1) 0.5f
        else level.toFloat() / scale.toFloat()
}