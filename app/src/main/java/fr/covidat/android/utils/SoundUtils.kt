/*
 * Copyright (c) 2020 Codefish <codefish@online.fr>
 * Software released under the terms of the CeCILL 2.1 license [https://cecill.info/]
 */

package fr.covidat.android.utils

import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.media.RingtoneManager
import android.net.Uri
import android.util.Log
import androidx.fragment.app.Fragment
import fr.covidat.android.service.loadGeneratorServiceConfig

fun Fragment.pickTone(title: String, requestCode: Int, existingUri: Uri? = null, type: Int = RingtoneManager.TYPE_NOTIFICATION) {
    val config = loadGeneratorServiceConfig(context!!)
    val intent = Intent(RingtoneManager.ACTION_RINGTONE_PICKER)
    intent.apply {
        putExtra(
            RingtoneManager.EXTRA_RINGTONE_TITLE,
            title
        )
        putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_SILENT, false)
        putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_DEFAULT, true)
        putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, type)
        if (config.generationTone != null) putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, existingUri)
        startActivityForResult(this, requestCode)
    }
}