/*
 * Copyright (c) 2020 Codefish <codefish@online.fr>
 * Software released under the terms of the CeCILL 2.1 license [https://cecill.info/]
 */

package fr.covidat.android.utils

import android.content.Context
import android.media.MediaPlayer
import android.net.Uri
import android.util.Log


object SoundPlayer {
    val mediaPlayer: MediaPlayer by lazy { MediaPlayer() }

    var active: Boolean = true
        set(value) {
            if (!value) stopSound()
            field = value
        }

    fun playSound(context: Context, uri: Uri) {
        if (!active) return
        stopSound()
        mediaPlayer.reset()
        mediaPlayer.setOnPreparedListener { it -> it.start() }
        try {
            mediaPlayer.setDataSource(context, uri)
            mediaPlayer.prepareAsync()
        } catch (e: Exception) {
            Log.e(javaClass.name, "Cannot play sound", e)
        }
    }

    fun stopSound() {
        if (mediaPlayer.isPlaying) mediaPlayer.stop()
    }
}