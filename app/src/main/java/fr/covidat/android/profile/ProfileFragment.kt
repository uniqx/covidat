/*
 * Copyright (c) 2020 Codefish <codefish@online.fr>
 * Software released under the terms of the CeCILL 2.1 license [https://cecill.info/]
 */

package fr.covidat.android.profile

import android.app.Dialog
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import com.tsongkha.spinnerdatepicker.DatePickerDialog
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder
import fr.covidat.android.R
import fr.covidat.android.model.PersonalData
import fr.covidat.android.model.loadPersonalData
import fr.covidat.android.model.save
import fr.covidat.android.service.GeneratorServiceFragment
import kotlinx.android.synthetic.main.fragment_profile.*
import java.util.*

/**
 * An activity to edit a profile with personal data
 */
class ProfileFragment : Fragment() {

    class FieldInfo(fragment: Fragment, val name: String) {
        val field = PersonalData::class.java.declaredFields.find { it.name == name }
        val viewID = fragment.resources.getIdentifier("${name}EditText", "id", "fr.covidat.android")
        val editText = fragment.view?.findViewById<EditText>(viewID)
        val editTextValue get() = editText!!.text.toString()
        val validator = PersonalData.javaClass.methods.find { it.name == "isValid${name[0].toUpperCase()}${name.substring(1)}" }

        init {
            field?.isAccessible = true
        }

        fun installValidator(fragment: Fragment) {
            val et = fragment.view?.findViewById<EditText>(viewID)
            et?.addTextChangedListener(object: TextWatcher {
                override fun afterTextChanged(s: Editable?) { et.error = validator!!.invoke(PersonalData, s.toString()) as String? }
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            })
        }
    }

    val fieldMapping: Map<String, FieldInfo> by lazy {
        PersonalData::class.java.declaredFields.map { it.name }.associateWith { FieldInfo(this, it) } }

    fun getFieldValue(name: String) = fieldMapping[name]!!.editTextValue

    var filledPersonalData
        get() = PersonalData(firstName = getFieldValue("firstName"),
            lastName = getFieldValue("lastName"),
            birthDate = getFieldValue("birthDate"),
            birthPlace = getFieldValue("birthPlace"),
            street =  getFieldValue("street"),
            city = getFieldValue("city"),
            zipcode = getFieldValue("zipcode"))
        set(value) {
            fieldMapping.forEach {
                it.value.editText?.setText(it.value.field?.get(value)?.toString())
            }
        }

    override fun onStart() {
        super.onStart()
        filledPersonalData = loadPersonalData(context!!)
    }

    override fun onStop() {
        super.onStop()
        filledPersonalData.save(context!!)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_profile, container, false)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fieldMapping.values.forEach { it.installValidator(this) }
        birthDateEditText.setOnClickListener {
            val dialog = BirthDateDialogFragment()
            dialog.setTargetFragment(this, 1)
            dialog.show(fragmentManager!!, "birthDatePicker")
        }
    }

    fun setBirthDate(year: Int, month: Int, day: Int) {
        birthDateEditText.setText("${day.toString().padStart(2, '0')}/${month.toString().padStart(2, '0')}/${year}")
    }

    class BirthDateDialogFragment: DialogFragment() {
        override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
            return SpinnerDatePickerDialogBuilder().run {
                val calendar = Calendar.getInstance()
                context(context)
                showTitle(true)
                showDaySpinner(true)
                maxDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
                calendar.add(Calendar.YEAR, -120) // we assume that nobody is more than 120 years old
                minDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))

                callback { view, year, month, day ->
                    (targetFragment as ProfileFragment).setBirthDate(year, month+1, day)  }
            }.build()
        }
    }
}
