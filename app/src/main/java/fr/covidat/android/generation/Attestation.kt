/*
 * Copyright (c) 2020 Codefish <codefish@online.fr>
 * Software released under the terms of the CeCILL 2.1 license [https://cecill.info/]
 */

package fr.covidat.android.generation

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.graphics.pdf.PdfDocument
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import android.graphics.*
import android.graphics.Bitmap
import android.net.Uri
import android.util.Log
import android.widget.Toast
import androidx.core.content.FileProvider
import androidx.core.graphics.applyCanvas
import com.google.gson.Gson
import fr.covidat.android.R
import fr.covidat.android.model.AttestationData
import java.io.File
import java.io.IOException
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

fun AttestationData.getQRText(): String =
    arrayOf(
        "Cree le: ${circumstances.day} a ${circumstances.hourMinute}",
        "Nom: ${personalData.lastName}",
        "Prenom: ${personalData.firstName}",
        "Naissance: ${personalData.birthDate} a ${personalData.birthPlace}",
        "Adresse: ${personalData.street} ${personalData.zipcode} ${personalData.city}",
        "Sortie: ${circumstances.day} a ${circumstances.hourMinute}",
        "Motifs: ${circumstances.reasonsText}"
    ).joinToString("; ")

class Attestation(val context: Context, val attestationData: AttestationData, val directory: File) {

    companion object {
        private val paint = Paint()
        private val bounds = Rect()

        const val DEFAULT_FONT_SIZE = 30
        const val MIN_FONT_SIZE = 10

        const val ATTESTATION_DIRECTORY = "attestations"
        const val PDF_FILENAME = "attestation.pdf"
        const val ATTESTATION_PNG_FILENAME = "attestation.png"
        const val QRCODE_PNG_FILENAME = "qrcode.png"
        const val DATA_FILENAME = "data.json"

        private fun setIdealFontSize(text: String, width: Int, height: Int, minSize: Int, defaultSize: Int) {
            paint.isAntiAlias = true
            paint.textSize = defaultSize.toFloat()
            paint.getTextBounds(text, 0, text.length, bounds)
            while (bounds.width() > width && bounds.height() > height && paint.textSize > minSize) {
                paint.textSize--
                paint.getTextBounds(text, 0, text.length, bounds)
            }
        }

        private fun Canvas.drawText(
            text: String,
            position: IntArray,
            defaultSize: Int = DEFAULT_FONT_SIZE,
            minSize: Int = MIN_FONT_SIZE
        ) {
            setIdealFontSize(text, position[2], position[3], minSize, defaultSize)
            this.drawText(text, (position[0] + DocumentPositions.offset[0]).toFloat(), (position[1] + DocumentPositions.offset[1]).toFloat(), paint)
        }

        private fun Canvas.drawBitmap(bitmap: Bitmap, position: IntArray) {
            drawBitmap(bitmap, (position[0] + DocumentPositions.offset[0]).toFloat(), (position[1] + DocumentPositions.offset[1]).toFloat(), paint)
        }

        fun getLatestAttestation(context: Context): Attestation? {
            val parentDir = context.applicationContext.filesDir.resolve(ATTESTATION_DIRECTORY)
            val dir = if (parentDir.exists()) parentDir.listFiles().maxBy { it.name } else null
            if (dir == null) return null
            else {
                val json = dir.resolve(DATA_FILENAME).readText()
                val data = try { Gson().fromJson(json, AttestationData::class.java) } catch (e: Exception) { null }
                if (data != null) return Attestation(context, data, dir) else return null
            }
        }

        val Long.iso8601: String get() {
            return SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US).let {
                it.setTimeZone(TimeZone.getTimeZone("UTC"))
                it.format(Date(this))
            }
        }

        fun create(context: Context, data: AttestationData): Attestation {
            val parentDir = context.applicationContext.filesDir.resolve(ATTESTATION_DIRECTORY)
            // create directory for the attestation
            val dir = parentDir.resolve(data.circumstances.date.iso8601)
            if (dir.mkdirs()) {
                val json = Gson().toJson(data)
                dir.resolve(DATA_FILENAME).writeText(json)
                return Attestation(context, data, dir)
            } else
                throw IOException("Cannot create directory $dir")
        }

        /** Keep only the directory of the latest attestation */
        fun removeOldAttestations(context: Context) {
            val parentDir = context.applicationContext.filesDir.resolve(ATTESTATION_DIRECTORY)
            if (parentDir != null) {
                val l = parentDir.listFiles().sortedBy { it.name }
                if (l.size > 1) {
                    val l2 = l.subList(0, l.size - 1)
                    l2.forEach { it.deleteRecursively() }
                }
            }
        }
    }



    fun generateQRCode(size: Pair<Int, Int>): Bitmap {
        val matrix = MultiFormatWriter().encode(
            attestationData.getQRText(),
            BarcodeFormat.QR_CODE, size.first, size.second
        )
        val bm = Bitmap.createBitmap(size.first, size.second, Bitmap.Config.ARGB_8888)
        val pixels = IntArray(size.first)
        for (j in 0.until(size.second)) {
            // write row by row in the bitmap
            for (i in 0.until(size.first))
                pixels[i] = if (matrix.get(i, j)) Color.BLACK else Color.WHITE
            bm.setPixels(pixels, 0, size.first, 0, j, size.first, 1)
        }
        return bm
    }



    fun drawForm(canvas: Canvas) {
        val options = BitmapFactory.Options()
        options.inScaled = false
        val bm = BitmapFactory.decodeResource(context.resources, R.raw.certificate, options)
        val d = attestationData
        canvas.apply {
            val rect = Rect(0,0, DocumentPositions.pageSize.first, DocumentPositions.pageSize.second)
            drawBitmap(bm, rect, rect, paint)
            mapOf(
                "${d.personalData.firstName} ${d.personalData.lastName}" to DocumentPositions.name,
                d.personalData.birthDate to DocumentPositions.birthDate,
                d.personalData.birthPlace to DocumentPositions.birthPlace,
                "${d.personalData.street} ${d.personalData.zipcode} ${d.personalData.city}" to DocumentPositions.address,
                "${d.circumstances.day}" to DocumentPositions.date,
                "${d.circumstances.hour}" to DocumentPositions.hour,
                "${d.circumstances.minute}" to DocumentPositions.minute
            ).forEach { drawText(it.key, it.value) }
            d.circumstances.reasons.forEach {
                drawText("x", DocumentPositions.reason[it]!!, 50)
            }
            drawText(d.personalData.city, DocumentPositions.place)

            val qr = generateQRCode(DocumentPositions.qr[2] to DocumentPositions.qr[3])
            drawBitmap(qr, DocumentPositions.qr)
            drawText("Date de création:", DocumentPositions.creationDateLabel, 20)
            drawText(
                "${d.circumstances.day} à ${d.circumstances.hourMinute}",
                DocumentPositions.creationDate, 20
            )
        }
    }

    fun drawLargeQRCode(canvas: Canvas) {
        val qr = generateQRCode(DocumentPositions.qr2[2] to DocumentPositions.qr2[3])
        canvas.drawBitmap(qr, DocumentPositions.qr2)
    }

    fun generatePDFAttestation() {
        val doc = PdfDocument()
        arrayOf(this::drawForm, this::drawLargeQRCode).forEachIndexed { i, gen ->
            val pageInfo = PdfDocument.PageInfo.Builder(
                DocumentPositions.pageSize.first,
                DocumentPositions.pageSize.second,
                i + 1
            ).create()
            val page = doc.startPage(pageInfo)
            gen(page.canvas)
            doc.finishPage(page)
        }
        directory.resolve(PDF_FILENAME).outputStream().use { doc.writeTo(it) }
    }

    fun generatePNGAttestation() {
        val bm = Bitmap.createBitmap(DocumentPositions.pageSize.first, DocumentPositions.pageSize.second, Bitmap.Config.ARGB_8888)
        bm.applyCanvas { drawForm(this) }
        val f = directory.resolve(ATTESTATION_PNG_FILENAME)
        f.outputStream().use { bm.compress(Bitmap.CompressFormat.PNG, 0, it) }

        val qr = generateQRCode(300 to 300)
        val f2 = directory.resolve(QRCODE_PNG_FILENAME)
        f2.outputStream().use { qr.compress(Bitmap.CompressFormat.PNG, 0, it) }
    }

    /** Generate the attestion for all the formats (and remove the old attestation) */
    fun generateAttestation() {
        generatePNGAttestation()
        generatePDFAttestation()
        removeOldAttestations(context)
    }

    fun loadAttestationBitmap(): Bitmap {
        directory.resolve(ATTESTATION_PNG_FILENAME).inputStream().use {
            return BitmapFactory.decodeStream(it)
        }
    }

    fun loadQRCodeBitmap(context: Context): Bitmap {
        directory.resolve(QRCODE_PNG_FILENAME).inputStream().use {
            return BitmapFactory.decodeStream(it)
        }
    }

    fun launchPDFIntent(context: Context, action: String) {
        val uri = FileProvider.getUriForFile(context,
            "fr.covidat.android.fileprovider", directory.resolve(PDF_FILENAME))

        val intent = Intent(action)
        intent.setDataAndType(uri, context.contentResolver.getType(uri))
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        if (action == Intent.ACTION_SEND) intent.putExtra(Intent.EXTRA_STREAM, uri)
        try {
            context.startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            Toast.makeText(context, "Pas d'activité disponible pour cette action", Toast.LENGTH_LONG).show()
        }
    }

    fun copyPDFAttestation(uri: Uri) {
        val outputStream = context.contentResolver.openOutputStream(uri)
        val inputStream = directory.resolve(Attestation.PDF_FILENAME).inputStream()
        inputStream?.use { val i = it; outputStream?.use { i.copyTo(it) } }
    }
}