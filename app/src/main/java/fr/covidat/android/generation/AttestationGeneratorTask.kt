/*
 * Copyright (c) 2020 Codefish <codefish@online.fr>
 * Software released under the terms of the CeCILL 2.1 license [https://cecill.info/]
 */

package fr.covidat.android.generation

import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.util.Log
import android.widget.Toast
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import fr.covidat.android.model.AttestationData
import java.io.Serializable
import java.lang.Exception

data class AttestationTaskResult(val generated: Boolean = false, val exception: Exception? = null) {
    companion object {
        /** Action used for broadcast */
        val NEW_ATTESTATION_ACTION = AttestationTaskResult::class.java.name + ".new_attestation"
    }

    override fun toString(): String {
        val state = mutableListOf<String>()
        if (generated) state.add("générée")
        if (exception != null) state.add(exception.message ?: "exception ${exception}")
        return "Attestation : ${state.joinToString(", ")}"
    }
}

open class AttestationGeneratorTask(val context: Context): AsyncTask<AttestationData, Void, AttestationTaskResult>() {
    var generated = false
    lateinit var attestation: Attestation

    override fun doInBackground(vararg params: AttestationData?): AttestationTaskResult {
        try {
            attestation = Attestation.create(context, params[0]!!)
            attestation.generateAttestation()
            generated = true
            return AttestationTaskResult(generated) // no exception
        } catch (e: Exception) {
            Log.e(javaClass.name, "Failure when generating the attestation", e)
            return AttestationTaskResult(exception = e)
        }
    }

    override fun onPostExecute(result: AttestationTaskResult) {
        if (result.exception == null) {
            val intent = Intent(AttestationTaskResult.NEW_ATTESTATION_ACTION)
            intent.putExtra("attestationData", attestation.attestationData as Serializable)
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
        }
        Toast.makeText(context, result.toString(), Toast.LENGTH_LONG).show()
    }
}