/*
 * Copyright (c) 2020 Codefish <codefish@online.fr>
 * Software released under the terms of the CeCILL 2.1 license [https://cecill.info/]
 */

package fr.covidat.android.generation

import fr.covidat.android.model.Reason

object DocumentPositions {

    val pageSize = 1652 to 2338
    val offset = arrayOf(10, 28)

    val reasonBasePos = intArrayOf(205, -1, 42, 42)

    fun r(a: Int): IntArray {
        val c = reasonBasePos.copyOf()
        c[1] = a
        return c
    }

    val reason = mapOf(
        Reason.travail to r(840),
        Reason.courses to r(979),
        Reason.sante to r(1093),
        Reason.famille to r(1193),
        Reason.sport to r(1349),
        Reason.judiciaire to r(1477),
        Reason.missions to r(1578)
    )
    
    val name = intArrayOf(400, 407, 1044, 38)
    val birthDate = intArrayOf(400, 472, 1044, 38)
    val birthPlace = intArrayOf(400, 540, 1044, 38)
    val address = intArrayOf(400, 608, 1044, 38)

    val place = intArrayOf(301, 1683, 1161, 38)

    val date = intArrayOf(257, 1753, 219, 32)
    val hour = intArrayOf(514, 1753, 72, 32)
    val minute = intArrayOf(615, 1753, 72, 32)

    val creationDateLabel = intArrayOf(1220, 1920, 300, 21)
    val creationDate = intArrayOf(1220, 1945, 300, 21)

    val qr = intArrayOf(1164, 1625, 300, 300)

    val qr2 = intArrayOf(400, 400, 800, 800)
}