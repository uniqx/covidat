/*
 * Copyright (c) 2020 Codefish <codefish@online.fr>
 * Software released under the terms of the CeCILL 2.1 license [https://cecill.info/]
 */

package fr.covidat.android.generation

import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import fr.covidat.android.*
import fr.covidat.android.model.*
import fr.covidat.android.profile.ProfileFragment
import kotlinx.android.synthetic.main.fragment_attestation_generator.*
import java.util.*
import java.util.prefs.PreferenceChangeListener

class AttestationGeneratorFragment : Fragment() {
    companion object {
        const val PERSONAL_DATA_REQUEST_ID = 1
    }

    var personalData: PersonalData? = null


    private fun getCheckedReasons(): EnumSet<Reason> {
        val reasons = EnumSet.noneOf(Reason::class.java)
        Reason.values().forEach {
            if (reasonsContainer.findViewWithTag<CheckBox>(it.name)?.isChecked == true)
                reasons.add(it)
        }
        return reasons
    }

    private fun refreshView() {
        personalDataTextView.text = if (personalData?.isValid ?: false)
            personalData.toString()
        else
            "Données personnelles incomplètes\nCliquez sur modifier pour les indiquer"
        generateButton.isEnabled = (personalData?.isValid == true && getCheckedReasons().size > 0)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_attestation_generator, container, false)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val reasons = loadReasons(context!!)
        Reason.values().forEach {
            val checkBox = CheckBox(context)
            checkBox.tag = it.name
            checkBox.text = it.description
            checkBox.isChecked = it in reasons
            checkBox.setOnClickListener { refreshView() }
            reasonsContainer.addView(checkBox)
        }

        personalDataModifyButton.setOnClickListener {
            val action = AttestationGeneratorFragmentDirections.gotoProfileFromMain()
            findNavController().navigate(action)
        }

        generateButton.setOnClickListener {
            val data = AttestationData.create(context!!, getCheckedReasons())
            val task = object: AttestationGeneratorTask(context!!) {
                override fun onPostExecute(result: AttestationTaskResult) {
                    super.onPostExecute(result)
                    if (result.generated) {
                        val action = AttestationGeneratorFragmentDirections.gotoAttestationFromMain()
                        findNavController().navigate(action)
                    }
                }
            }
            task.execute(data)
            getCheckedReasons().save(context!!)
        }
    }

    fun refreshPersonalData() {
        personalData = loadPersonalData(context!!)
        refreshView()
    }

    val prefListener = SharedPreferences.OnSharedPreferenceChangeListener { sharedPreferences, key ->
        if (key == "personalData") refreshPersonalData()
    }

    override fun onStart() {
        super.onStart()
        // prefill the personal data
        refreshPersonalData()
        PreferenceManager.getDefaultSharedPreferences(context!!).registerOnSharedPreferenceChangeListener(prefListener)
    }

    override fun onStop() {
        super.onStop()
        PreferenceManager.getDefaultSharedPreferences(context!!).unregisterOnSharedPreferenceChangeListener(prefListener)
    }

}