/*
 * Copyright (c) 2020 Codefish <codefish@online.fr>
 * Software released under the terms of the CeCILL 2.1 license [https://cecill.info/]
 */

package fr.covidat.android.attic


import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import fr.covidat.android.R
import kotlinx.android.synthetic.main.fragment_copy_attestation.*

/**
 * The fragment for copy attestation config.
 */
class CopyAttestationFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_copy_attestation, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        copyAttestationCheckBox.setOnClickListener {
            val filename = filenameEditText.text.toString()
            val activated = copyAttestationCheckBox.isChecked
            var copyConfig = CopyConfig(activated, filename)
            if (activated && !copyConfig.isReallyActivated(activity!!)) {
                copyAttestationCheckBox.isChecked = false
                ActivityCompat.requestPermissions(activity!!, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), 1)
                copyConfig = copyConfig.copy(activated = false)
            }
            // todo: check filename
            copyConfig.save(context!!)
        }
    }

    override fun onStart() {
        super.onStart()
        val copyConfig = loadCopyConfig(context!!)
        copyAttestationCheckBox.isChecked = copyConfig.isReallyActivated(activity!!)
        filenameEditText.setText(copyConfig.filename)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.getOrNull(0) == PackageManager.PERMISSION_GRANTED) {
            copyAttestationCheckBox.isChecked = true
            CopyConfig(true, filename = filenameEditText.text.toString()).save(context!!)
        }
    }


}
