/*
 * Copyright (c) 2020 Codefish <codefish@online.fr>
 * Software released under the terms of the CeCILL 2.1 license [https://cecill.info/]
 */

package fr.covidat.android.attic

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.os.Environment
import android.preference.PreferenceManager
import androidx.core.content.ContextCompat
import com.google.gson.Gson
import fr.covidat.android.generation.Attestation
import java.lang.Exception

private val gson = Gson()

const val DEFAULT_COPIED_FILENAME = "covid19-attestation.pdf"

data class CopyConfig(val activated: Boolean = false, val filename: String = DEFAULT_COPIED_FILENAME) {

    /** Check also if the writing permission is granted */
    fun isReallyActivated(activity: Activity): Boolean {
        return activated && ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
    }
}

fun CopyConfig.save(context: Context) {
    val editor = PreferenceManager.getDefaultSharedPreferences(context).edit()
    editor.putString("copyConfig", gson.toJson(this))
    editor.commit()
}

fun loadCopyConfig(context: Context): CopyConfig {
    val s = PreferenceManager.getDefaultSharedPreferences(context).getString("reasons", null)
    return s?.let { try {
        gson.fromJson(it, CopyConfig::class.java)
    } catch (e: Exception) { null } } ?: CopyConfig()
}

fun Attestation.copyPDFAttestation(): Boolean {
    val copyConfig = loadCopyConfig(context)
    if (copyConfig.activated) {
        val targetFile = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
            .resolve(copyConfig.filename)
        //copyPDFAttestation(targetFile)
        return true
    } else
        return false
}