/*
 * Copyright (c) 2020 Codefish <codefish@online.fr>
 * Software released under the terms of the CeCILL 2.1 license [https://cecill.info/]
 */

package fr.covidat.android.viewer


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import fr.covidat.android.generation.Attestation

/**
 * A fragment displaying the QR code of the latest attestation.
 */
class QRCodeFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val bm = Attestation.getLatestAttestation(context!!)?.loadQRCodeBitmap(context!!)
        return if (bm != null) {
            val imageView = ImageView(context)
            imageView.setImageBitmap(bm)
            imageView.scaleType = ImageView.ScaleType.FIT_CENTER
            imageView
        } else {
            val tv = TextView(context)
            tv.text = "Pas encore d'attestation générée"
            tv
        }
    }


}
